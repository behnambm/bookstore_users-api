package app

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/behnambm/bookstore_users-api/logger"
)

var (
	router = gin.Default()
)

func StartApplication() {
	mapUrls()
	gin.SetMode(gin.DebugMode)
	logger.Info("About to start the application")
	router.Run(":8080")
}
