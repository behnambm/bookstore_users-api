// User DAO(Data Access Object)
// The only place that interacts directly with the database.

package users

import (
	"fmt"
	"gitlab.com/behnambm/bookstore_users-api/datasources/mysql/users_db"
	"gitlab.com/behnambm/bookstore_users-api/logger"
	"gitlab.com/behnambm/bookstore_users-api/utils/date_utils"
	"gitlab.com/behnambm/bookstore_users-api/utils/errors"
)

const (
	queryGetUser      = "SELECT id, first_name, last_name, email, date_created, status FROM users WHERE id=?;"
	queryInsertUser   = "INSERT INTO users(first_name, last_name, email, date_created, password, status) VALUES (?, ?, ?, ?, ?, ?);"
	queryUpdateUser   = "UPDATE users SET first_name=?, last_name=?, email=? WHERE id=?;"
	queryDeleteUser   = "DELETE FROM users WHERE id=?;"
	queryFindByStatus = "SELECT id, first_name, last_name, email, date_created, status FROM users WHERE status=?;"
)

func (user *User) Get() *errors.RestErr {
	stmt, err := users_db.Client.Prepare(queryGetUser)
	if err != nil {
		logger.Error("error in preparing statement", err)
		return errors.NewInternalServerError("database error")
	}
	defer stmt.Close()

	result := stmt.QueryRow(user.Id)
	scnErr := result.Scan(&user.Id, &user.FirstName, &user.LastName, &user.Email, &user.DateCreated, &user.Status)
	if scnErr != nil {
		logger.Error("error in scanning", scnErr)
		return errors.NewInternalServerError("database error")
	}
	return nil
}

func (user *User) Save() *errors.RestErr {
	stmt, err := users_db.Client.Prepare(queryInsertUser)
	if err != nil {
		logger.Error("error in preparing statement", err)
		return errors.NewInternalServerError("database error")
	}
	defer stmt.Close()

	user.DateCreated = date_utils.GetNowString()

	insertResult, saveErr := stmt.Exec(user.FirstName, user.LastName, user.Email, user.DateCreated, user.Password, user.Status)
	if saveErr != nil {
		logger.Error("error in saving user", saveErr)
		return errors.NewInternalServerError("database error")
	}
	userId, err := insertResult.LastInsertId()
	if err != nil {
		logger.Error("error in getting last insert ID", err)
		return errors.NewInternalServerError("database error")
	}
	user.Id = userId
	return nil
}

func (user *User) Update() *errors.RestErr {
	stmt, err := users_db.Client.Prepare(queryUpdateUser)
	if err != nil {
		logger.Error("error in preparing statement", err)
		return errors.NewInternalServerError("database error")
	}
	defer stmt.Close()

	_, execErr := stmt.Exec(user.FirstName, user.LastName, user.Email, user.Id)
	if execErr != nil {
		logger.Error("error in executing update query", execErr)
		return errors.NewInternalServerError("database error")
	}
	return nil
}

func (user *User) Delete() *errors.RestErr {
	stmt, err := users_db.Client.Prepare(queryDeleteUser)
	if err != nil {
		logger.Error("error in executing update query", err)
		return errors.NewInternalServerError("database error")
	}
	defer stmt.Close()
	res, execErr := stmt.Exec(user.Id)
	rowsCount, rowsErr := res.RowsAffected()
	if rowsErr != nil {
		logger.Error("error in getting rows affected", rowsErr)
		return errors.NewInternalServerError("database error")
	}
	if rowsCount == 0 {
		return errors.NewNotFoundError("user not found")
	}
	if execErr != nil {
		logger.Error("error in executing query", execErr)
		return errors.NewInternalServerError("database error")
	}
	return nil
}

func (user *User) FindByStatus(status string) ([]User, *errors.RestErr) {
	stmt, stmtErr := users_db.Client.Prepare(queryFindByStatus)
	if stmtErr != nil {
		logger.Error("error in preparing statement", stmtErr)
		return nil, errors.NewInternalServerError("database error")
	}

	rows, qryErr := stmt.Query(status)
	if qryErr != nil {
		logger.Error("error in preparing statement", qryErr)
		return nil, errors.NewInternalServerError("database error")
	}
	results := make([]User, 0)
	for rows.Next() {
		var user User
		scnErr := rows.Scan(&user.Id, &user.FirstName, &user.LastName, &user.Email, &user.DateCreated, &user.Status)
		if scnErr != nil {
			logger.Error("error in scanning", scnErr)
			return nil, errors.NewInternalServerError("database error")
		}
		results = append(results, user)
	}
	if len(results) == 0 {
		return nil, errors.NewNotFoundError(fmt.Sprintf("no users matching status %s", status))
	}
	return results, nil
}
