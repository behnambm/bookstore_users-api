package services

import (
	"gitlab.com/behnambm/bookstore_users-api/domain/users"
	"gitlab.com/behnambm/bookstore_users-api/utils/crypto_utils"
	"gitlab.com/behnambm/bookstore_users-api/utils/date_utils"
	"gitlab.com/behnambm/bookstore_users-api/utils/errors"
	"strings"
)

const (
	userStatusActive = "ACTIVE"
)

var (
	UserService userServiceInterface = &userService{}
)

type userService struct{}

type userServiceInterface interface {
	GetUser(int64) (*users.User, *errors.RestErr)
	CreateUser(users.User) (*users.User, *errors.RestErr)
	UpdateUser(bool, users.User) (*users.User, *errors.RestErr)
	DeleteUser(int64) *errors.RestErr
	Search(string) (users.Users, *errors.RestErr)
}

func (s *userService) CreateUser(user users.User) (*users.User, *errors.RestErr) {
	if err := user.Validate(); err != nil {
		return nil, err
	}
	user.Status = userStatusActive
	user.DateCreated = date_utils.GetNowDBFormat()
	user.Password = crypto_utils.GetMd5(user.Password)
	if err := user.Save(); err != nil {
		return nil, err
	}
	return &user, nil
}

func (s *userService) GetUser(userId int64) (*users.User, *errors.RestErr) {
	if userId <= 0 {
		return nil, errors.NewBadRequestError("invalid user id")
	}
	user := users.User{Id: userId}
	if err := user.Get(); err != nil {
		return nil, err
	}
	return &user, nil
}

func (s *userService) UpdateUser(isPartial bool, user users.User) (*users.User, *errors.RestErr) {
	current, getErr := s.GetUser(user.Id)
	if getErr != nil {
		return nil, getErr
	}

	if isPartial {
		if strings.TrimSpace(user.FirstName) != "" {
			current.FirstName = user.FirstName
		}
		if strings.TrimSpace(user.LastName) != "" {
			current.LastName = user.LastName
		}
		if strings.TrimSpace(user.Email) != "" {
			current.Email = user.Email
		}
	} else {
		current.FirstName = user.FirstName
		current.LastName = user.LastName
		current.Email = user.Email
	}
	if updateErr := current.Update(); updateErr != nil {
		return nil, updateErr
	}

	return current, nil
}

func (s *userService) DeleteUser(userId int64) *errors.RestErr {
	user := users.User{Id: userId}
	return user.Delete()
}

func (s *userService) Search(status string) (users.Users, *errors.RestErr) {
	dao := &users.User{}
	return dao.FindByStatus(status)
}
