package mysql_utils

import (
	"github.com/VividCortex/mysqlerr"
	"github.com/go-sql-driver/mysql"
	"gitlab.com/behnambm/bookstore_users-api/utils/errors"
	"strings"
)

const (
	dbErrorNowRows = "no rows in result set"
)

func ParseError(err error) *errors.RestErr {
	sqlErr, ok := err.(*mysql.MySQLError)
	if !ok {
		if strings.Contains(err.Error(), dbErrorNowRows) {
			return errors.NewBadRequestError("no record matching")
		}
		return errors.NewInternalServerError("error parsing database response")
	}
	switch sqlErr.Number {
	case mysqlerr.ER_DUP_ENTRY:
		return errors.NewBadRequestError("duplicate data")
	}
	return errors.NewInternalServerError("error processing request " + err.Error())
}
