package users_db

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"os"
)

const (
	mysqlUsersDBUsername = "MYSQL_USERS_DB_USERNAME"
	mysqlUsersDBPassword = "MYSQL_USERS_DB_PASSWORD"
	mysqlUsersDBHost     = "MYSQL_USERS_DB_HOST"
	mysqlUsersDBSchema   = "MYSQL_USERS_DB_SCHEMA"
)

var (
	Client   *sql.DB
	username = os.Getenv(mysqlUsersDBUsername)
	password = os.Getenv(mysqlUsersDBPassword)
	host     = os.Getenv(mysqlUsersDBHost)
	schema   = os.Getenv(mysqlUsersDBSchema)
)

func init() {
	var err error
	dataSourceName := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8", username, password, host, schema)
	Client, err = sql.Open("mysql", dataSourceName)
	if err != nil {
		panic(err)
	}
	if err = Client.Ping(); err != nil {
		panic(err)
	}
	log.Println("database successfully connected")
}
